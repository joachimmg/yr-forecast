<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use App\Yr\Forecast;
use App\Yr\Forecast\Tabular;
use App\Yr\Forecast\Tabular\Time\AbstractUnit;
use App\Yr\Forecast\Tabular\Time\Symbol;
use App\Yr\Forecast\Tabular\Time\Temperature;
use App\Yr\Forecast\Tabular\Time\WindDirection;
use App\Yr\Forecast\Tabular\Time\WindSpeed;

require __DIR__ . '/vendor/autoload.php';

class TextForcast {
    const HOUR_FORMAT = 'H (h a)';

    private $forecast;

    public function __construct(Tabular $t) {
        $this->forecast = $t; 
    }

    /**
     * «Tonight Southeasterly light breeze, cloudy, temperature -8 to  -9 degrees»
     * @author Helge Tangen
     */
    public function json(): string {
        $time = $this->forecast->getVariations()->getTime();

        $arr = [
            "from"      => $this->forecast->getFrom()->format('Y-m-d H:i'),
            "until"     => $this->forecast->getUntil()->format("Y-m-d H:i"),
            "initial"   => [],
            "changes"   => [],
            "credit"    => $this->forecast->getForecast()->getCredit()
        ];
        foreach ($time as $ent) {
            $arr['initial'][] = (string)$ent;
        }

        $vars = $this->forecast->getVariations()->filter(function($e, $p, $n) {
            return ($p == null ? true : $e->thresholdDiff($p->getEntity(get_class($e))));
        });
        
        foreach ($vars as $var) {
            $changes = [
                "at" => [
                    "time" => $var->getTime()->getFrom()->format('Y-m-d H:i'),
                    "period" => $this->when($var->getTime()->getFrom())
                ],
                "to" => [],
            ];
            $var->operate(function($e, $p, $n) use (&$changes) {
                $changes['to'][] = (string)$e;
            });

            $arr['changes'][] = $changes;
        }

        return json_encode($arr);
    }

    public function when(\DateTimeInterface $d): string {
        $a = DateTime::createFromFormat('d/m/Y', $d->format('d/m/Y'));
        $b = DateTime::createFromFormat('d/m/Y', (new \DateTime())->format('d/m/Y'));
        $diff = $b->diff($a);

        if ($diff->invert && $diff->days == 1)
            return $this->period($d, "yesterday");
        elseif (!$diff->invert && $diff->days == 1)
            return $this->period($d, "tomorrow");
        elseif ($diff->days == 0)
            return $this->period($d, "today");

        return sprintf("the %s at %s of %s %y ",
            $this->period($d),
            $d->format('dd'), $d->format('m'), $d->format('YY')
        );
    }

    public function period(\DateTimeInterface $d, ?string $when = null): string {
        $hour = (int)$d->format('H');
        if ($hour >= 6 && $hour <= 11)
            return ($when == "today" ? "this" : $when ) . " morning";
        elseif ($hour == 12)
            return "at noon";
        elseif ($hour > 12 && $hour < 18)
            return ($when == "today" ? "this" : $when) . " afternoon";
        elseif ($hour >= 18 && $hour < 24)
            return ($when == "today" ? "tonight" : $when . " evening");
        elseif ($hour == 0)
            return ($when == "tomorrow" ? "at" : ($when == "today" ? "last" : $when)) . " midnight";
        else
            return ($when == "tomorrow" ? "this" : ($when == "today" ? "last" : $when)) . " night";
    }

    public function since(\DateTimeInterface $d): string {
    
    }
}

$url = 'https://www.yr.no/place/Norway/Troms/Tromsø/Tromsø/forecast_hour_by_hour.xml';

echo '<pre>';
$forecast = new Forecast($url);

$range = $forecast->getTabular()->getBetween(
    $forecast->getSunset(), $forecast->getSunrise()->add(new \DateInterval('P1D'))
);

$tf = new TextForcast($range);
echo $tf->json();
?>
