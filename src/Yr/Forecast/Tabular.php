<?php

namespace App\Yr\Forecast;

use App\Yr\Forecast;
use App\Yr\Forecast\Tabular\Statistics;
use App\Yr\Forecast\Tabular\Time;
use App\Yr\Forecast\Tabular\Variations;

/**
 * Holds the forecast data in Time-objects. 
 *
 * Use the ```getBetween``` option to limit the results.
 *
 * Class also makes a simple statistic on the forecast
 * for the periode and a variation that will exclude
 * repetitive forecast data.
 *
 * @author Joachim M. Giæver (joachim[]giaver.org)
 */
class Tabular implements \IteratorAggregate {

    private $time = [];
    private $stats;
    private $variations;
    private $forecast;

    /**
     * @param \SimpleXMLElement $xml The xml part holding the time objects, can be null
     */
    public function __construct($forecast, ?\SimpleXMLElement $xml) {
        $this->forecast = $forecast;
        $this->stats = new Statistics();

        if ($xml != null) {

            foreach ($xml->time as $time)
                $this->addTime(new Time($time));
            
            $this->makeVariations($time);
        }
    }

    public function getForecast(): Forecast {
        return $this->forecast;
    }

    /**
     * Add a Time-object to the tabular
     *
     * @return Tabular
     */
    protected function addTime(Time $time): self {
        $this->time[] = $time;
        $this->stats->analyse($time);
        return $this;
    }

    /**
     * Get statistics for the Time-object collection
     *
     * @return Statistics
     */
    public function getStatistics(): Statistics {
        return $this->stats;
    }

    protected function makeVariations(): self {
        $this->variations = new Variations($this->time);
        return $this;
    }

    /**
     * Remove superfluous weather data.
     *
     * Checks if the data in the Time-object differs from
     * the current Time-object and returns the unique data
     *
     * @return Variations
     */
    public function getVariations(): Variations {
        return $this->variations;
    }

    /**
     * Filter data between a certain periode, e.g
     * ```
     * $forcast->between(
     *  $forcast->getSunset(),
     *  $forecast->getSunrise()->add(
     *      new \DateInterval('P1D')
     *  )
     * );
     * ```
     * to only show from sunset today unti sunrise tomorrow
     *
     * @return Tabular with new collection
     */
    public function getBetween(\DateTimeInterface $from, \DateTimeInterface $until): self {
        $n = new Tabular($this->forecast, null);

        foreach ($this as $time)
            if ($time->getFrom() >= $from && $time->getUntil() <= $until)
                $n->addTime($time);

        $n->makeVariations(); 
        return $n;
    }

    /**
     * Return the time this tabular is from
     * 
     * @return \DateTimeInterface The from time
     */
    public function getFrom(): \DateTimeInterface {
        return current($this->time)->getFrom();
    }

    /**
     * Return the time this tabular is until
     * 
     * @return \DateTimeInterface The until time
     */
    public function getUntil(): \DateTimeInterface {
        return current(array_reverse($this->time))->getUntil();
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator(): ?\Generator {
        foreach ($this->time as $time)
            yield $time;

        return null;
    }
}
