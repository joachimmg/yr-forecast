<?php

namespace App\Yr\Forecast;

/**
 * Get information about the location,
 * such as name, timezone and geodata
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 */
final class Location {
    private $name;
    private $type;
    private $country;
    private $tz;
    private $loc = [];

    /**
     * @param \SimpleXMLElement $xml The xml-part containing the location data
     */
    public function __construct(\SimpleXMLElement $xml) {
        $this->name = (string)$xml->name;
        $this->type = (string)$xml->type;
        $this->country = (string)$xml->country;
        $this->tz = (string)$xml->timezone['id'];
        $this->loc = [
            'lat' => (float)$xml->location['latitude'],
            'lng' => (float)$xml->location['longitude'],
            'alt' => (float)$xml->location['altitude']
        ];
    }

    /**
     * Get the location name, e.g «Tromsø»
     *
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Get the location type, e.g «City - large town»
     *
     * @return string
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * Get the location country
     *
     * @return string
     */
    public function getCountry(): string {
        return $this->country;
    }

    /**
     * Get the location timezone
     *
     * @return \DateTimeZone
     */
    public function getTimeZone(): \DateTimeZone {
        return new \DateTimeZone($this->tz);
    }

    /**
     * Returns the geodata, in the format
     * ```
     * [
     *      'lat' => (float),
     *      'lng' => (float),
     *      'alt' => (float),
     * ]
     * ```
     *
     * @return array
     */
    public function getGeoData(): array {
        return $this->loc;
    }
}

?>
