<?php

namespace App\Yr\Forecast\Tabular;

use App\Yr\Forecast\Tabular\Time\AbstractUnit;
use App\Yr\Forecast\Tabular\Time\CustomUnit;
use App\Yr\Forecast\Tabular\Time\Temperature;
use App\Yr\Forecast\Tabular\Time\WindSpeed;

/**
 * Make simple statistic on analysed time objects,
 * such as highest/lowest wind speed and temperature,
 * average wind speed and temperature etc.
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 */
class Statistics {
    private $temp = [];
    private $wind = [];
    private $count = 0;
    private $symbol = [];

    public function __construct() {
        $this->temp = $this->struct();
        $this->wind = $this->struct();
    }

    private function struct(): array {
        return [
            'high' => null,
            'low' => null,
            'mean' => null
        ];
    }

    /**
     * Analyse a single Time-object
     *
     * @param Time $t The time object.
     * @return Statistics
     */
    public function analyse(Time $t): self {
        $this->analyseHihgLow($t->getTemperature());
        $this->analyseHihgLow($t->getWindSpeed());

        $this->temp['mean'] = $this->temp['mean'] == null ? $t->getTemperature() : $this->temp['mean']->add($t->getTemperature());
        $this->wind['mean'] = $this->wind['mean'] == null ? $t->getWindSpeed() : $this->wind['mean']->add($t->getWindSpeed());

        $symboldId = $t->getSymbol()->getNumber();

        if (!isset($this->symbol[$symboldId]))
            $this->symbol[$symboldId] = [
                'symbol' => $t->getSymbol()->getName(),
                'count' => 1,
                'avg' => 0,
            ];
        else
            $this->symbol[$symboldId]['count']++;

        $this->count++;
        return $this;
    }

    public function getAverageTemperature(): AbstractUnit {
        return $this->temp['mean']->div(
            new CustomUnit($this->count, $this->temp['mean']->getUnit())
        );
    }

    public function getAverageWindSpeed(): AbstractUnit {
        return $this->wind['mean']->div(
            new CustomUnit($this->count, $this->wind['mean']->getUnit())
        );
    }

    public function getHighestTemperature(): Temperature {
        return $this->temp['high'];
    }

    public function getLowestTemperature(): Temperature {
        return $this->temp['low'];
    }

    public function getHighestWindspeed(): WindSpeed {
        return $this->wind['high'];
    }

    public function getLowestWindSpeed(): WindSpeed {
        return $this->wind['low'];
    }

    public function getAverageSymbols(): array {
        if ($s = current($this->symbol))
           if ($s != null && $s['avg'] instanceof CustomUnit)
               return $this->symbol;

        array_walk($this->symbol, function(array &$symbol) {
            $symbol['avg'] = new CustomUnit($symbol['count'] / $this->count * 100, '%');
        });

        usort($this->symbol, function(array $a, array $b) {
            return (int)($a['avg']->sub($b['avg']))->getValue();
        });

        return $this->symbol;
    }

    public function getMostCommonSymbol(): array {
        return current(array_reverse($this->getAverageSymbols()));
    }

    private function analyseHihgLow(AbstractUnit $au): self {
        $unit = null;

        if ($au instanceof Temperature)
            $unit = &$this->temp;
        elseif ($au instanceof WindSpeed)
            $unit = &$this->wind;
        else
            return $this;

        if ($unit['low'] == null || $au->diff($unit['low']) <= 0)
            $unit['low'] = $au;

        if ($unit['high'] == null || $au->diff($unit['high']) >= 0)
            $unit['high'] = $au;

        return $this;
    }
}
?>
