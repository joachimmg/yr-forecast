<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Temperature
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 */
class Temperature extends AbstractUnit implements ConvertableInterface {
    const UNIT_CELCIUS      = 'celcius';
    const UNIT_FAHRENHEIT   = 'fahrenheit';
    const UNIT_KELVIN       = 'kelvin';

    /**
     * @param \SimpleXMLElement $xml XML containing the temperature
     */
    public function __construct(?\SimpleXMLElement $xml) {
        parent::__construct(
            (float)$xml['value'],
            (string)$xml['unit']
        );
    }

    /**
     * {@inheritDoc}
     * @todo support conversion from other types, not just celcius
     */
    public function convertTo(string $unit): ConvertableInterface {
        switch ($unit) {
        case self::UNIT_FAHRENHEIT:
            return $this->mul(new CustomUnit(
                9/5, $unit
            ))->add(new CustomUnit(
                32, $unit
            ));
        case self::UNIT_KELVIN:
            return $this->add(new CustomUnit(
                273.15, $unit
            ));
        }
    }

    public function thresholdDiff(DiffInterface $e): bool {
        return abs($this->diff($e)/100) > 1;
    }

    /**
     * Return the ⁰X symbol
     */
    public function getDegree(): string {
        return sprintf("⁰%s", strtoupper(substr($this->getUnit(), 0, 1)));
    }

    public function __toString(): string {
        return sprintf(
            "%s: %d %s",
            basename(str_replace("\\", DIRECTORY_SEPARATOR, get_class($this))),
            $this->getValue(),
            $this->getDegree()
        );
    }
}

