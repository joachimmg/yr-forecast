<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Implemented on units that can be converted, 
 * such as wind speed and temperature.
 */
interface ConvertableInterface {
    /**
     * Convert the to a different unit
     *
     * @param string $unit The unit to convert to, eg UNIT_FTS
     */
    public function convertTo(string $unit): self;
}
?>
