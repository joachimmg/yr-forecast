<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Custom unit, only used to operate on units of same kind
 *
 * Classes should transform this (see {@AbstractUnit}) to
 * the correct class again after operation.
 */
class CustomUnit extends AbstractUnit {
    // Nothing extra here
    function thresholdDiff(DiffInterface $e): bool {
        return $this->diff($e) == 0;
    }
}

?>
