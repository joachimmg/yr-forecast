<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Airpressure
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 * @todo Conversion
 */
class Pressure extends AbstractUnit {
    const NORMAL_PRESSURE = 1015.0;

    /**
     * @param \SimpleXMLElement $xml XML containing the pressure
     */
    public function __construct(\SimpleXMLElement $xml) {
        parent::__construct(
            (float)$xml['value'],
            (string)$xml['unit']
        );
    }

    /**
     * Check if the pressure is below normal pressure
     *
     * @return bool
     */
    public function isLowPressure(): bool {
        return $this->getValue() < self::NORMAL_PRESSURE;
    }

    /**
     * Check if the pressure is above normal pressure
     *
     * @return bool
     */
    public function isHighPressure(): bool {
        return $this->getValue() > self::NORMAL_PRESSURE;
    }

    public function thresholdDiff(DiffInterface $e): bool {
        return $this->isLowPressure() != $e->isLowPressure();
    }
}

