<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Contains the sky data, e.g «Clody» etc
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 */
class Symbol implements DiffInterface, \JsonSerializable {

    private $number;
    private $numberEx;
    private $name;
    private $var;

    /**
     * @param \SimpleXMLElement $xml XML containing the symbol data
     */
    public function __construct(\SimpleXMLElement $xml) {
        $this->number = (int)$xml['number'];
        $this->numberEx = (int)$xml['numberEx'];
        $this->name = (string)$xml['name'];
        $this->var = (string)$xml['var'];
    }

    /**
     * Retuns the type identifier
     *
     * @return int
     */
    public function getNumber(): int {
        return $this->number;
    }

    /**
     * Returns the name, e.g «clody».
     *
     * @return string
     */
    public function getName(): string {
        return strtolower($this->name);
    }

    /**
     * Return the var-variable
     *
     * @return string
     */
    public function getVar(): string {
        return $this->var;
    }

    /**
     * {@inheritDoc}
     */
    public function diff(DiffInterface $s): int {
        if ($s instanceof Symbol)
            return $this->number - $s->getNumber();

        return 0;
    }

    public function thresholdDiff(DiffInterface $e): bool {
        return $this->diff($e) != 0;
    }

    public function jsonSerialize(): string {
        return $this->__toString();
    }

    public function __toString(): string {
        return sprintf(
            "%s: %s (%d)", 
            basename(str_replace("\\", DIRECTORY_SEPARATOR, get_class($this))),
            $this->name,
            $this->number
        );
    }
}
