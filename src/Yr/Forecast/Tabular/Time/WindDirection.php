<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Wind direction
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 */
class WindDirection extends AbstractUnit{
    const DEFAULT_VARIANCE = 22.5;
    private $name;

    /**
     * @param \SimpleXMLElement $xml XML element containing the wind direction
     */
    public function __construct(\SimpleXMLElement $xml) {
        parent::__construct(
            (float)$xml['deg'],
            (string)$xml['code']
        );

        $this->name = (string)$xml['name'];
    }

    /**
     * Returns the wind direction in full,
     * e.g «Northeast».
     *
     * @return string
     */
    public function getName(): string {
        return strtolower($this->name);
    }

    /**
     * Returns a direction on the compass, not
     * in degree, but in interger between 1 - 16, 
     * each explining which spectre.
     *
     * E.g 
     *  1: North,
     *  2: North-northeast,
     *  3: North-east,
     *  [...]
     *  9: South
     * etc..
     *
     * @return int compass spectre
     */
    public function getDirection(): int {
        return (($this->getValue()-11.5) / 22.5);
    }

    /**
     * {@inheritDoc}
     */
    public function diff(DiffInterface $d): int {
        if (parent::diff($d))
            return $this->getDirection() - $d->getDirection();
        return 0;
    }

    public function thresholdDiff(DiffInterface $e): bool {
        return $this->diff($e) != 0;       
    }

    public function __toString(): string {
        return sprintf("%s: %s (%01.1f⁰)",
            basename(str_replace("\\", DIRECTORY_SEPARATOR, get_class($this))),
            $this->getName(), $this->getValue()
        );
    }
}
