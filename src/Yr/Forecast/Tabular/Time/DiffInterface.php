<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Defines that an entity can be checked for differences
 * agains another entity.
 */
interface DiffInterface {
    /**
     * Check for differences with another DiffInterface.
     * The method should check that the objects is the same.
     *
     * @param DiffInterface $e The object to check agains
     */
    public function diff(DiffInterface $e): int;
    public function thresholdDiff(DiffInterface $e): bool;
}

