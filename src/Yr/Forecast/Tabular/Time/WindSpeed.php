<?php

namespace App\Yr\Forecast\Tabular\Time;

/**
 * Wind speed
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 */
class WindSpeed extends AbstractUnit implements ConvertableInterface {
    const DEFAULT_VARIANCE = (4.9 / 3.5);

    const UNIT_MPS = 'mp/s';
    const UNIT_FTS = 'ft/s';
    const UNIT_KMH = 'km/h';
    const UNIT_KNOTS = 'knots';

    private $name;

    /**
     * @param \SimpleXMLElement $xml XML containing the wind spedd
     */
    public function __construct(\SimpleXMLElement $xml){
        parent::__construct(
            (float)$xml['mps'], self::UNIT_MPS
        );

        $this->name = (string)$xml['name'];
    }

    /**
     * {@inheritDoc}
     * @todo Support conversion from other types, not just mps
     */
    public function convertTo(string $unit): ConvertableInterface {
        switch ($unit) {
        case self::UNIT_KNOTS:
            return $this->mul(new CustomUnit(1.9438445, $unit))->setName($this->getName());
        case self::UNIT_FTS:
            return $this->mul(new CustomUnit(3.28084, $unit))->setName($this->getName());
        case self::UNIT_KMH:
            return $this->mul(new CustomUnit(3.6, $unit))->setName($this->getName());
        }
    }

    /**
     * Returns the wind name, e.g «light breeze»
     */
    public function getName(): string {
        return strtolower($this->name);
    }

    /**
     * Used on conversion
     */
    protected function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function thresholdDiff(DiffInterface $e): bool {
        return $this->getName() != $e->getName();
    }

    public function __toString(): string {
        return sprintf(
            '%s: %s (%01.1f %s)',
            basename(str_replace('\\', DIRECTORY_SEPARATOR, get_class($this))),
            $this->getName(),
            $this->getValue(),
            $this->getUnit()
        );
    }
}

