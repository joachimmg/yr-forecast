<?php

namespace App\Yr\Forecast\Tabular;

use App\Yr\Forecast\Tabular\Time\Pressure;
use App\Yr\Forecast\Tabular\Time\Symbol;
use App\Yr\Forecast\Tabular\Time\Temperature;
use App\Yr\Forecast\Tabular\Time\WindDirection;
use App\Yr\Forecast\Tabular\Time\WindSpeed;

/**
 * Forecast data witin a time period
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 */

final class Time implements TimeInterface, \IteratorAggregate {

    private $from;
    private $until;
    private $period;
    private $symbol;
    private $windDirection;

    /**
     * @param \SimpleXMLElement $xml The xml holding the time-object
     */
    public function __construct(\SimpleXMLElement $xml) {
        $this->from = new \DateTimeImmutable($xml['from']);
        $this->until = new \DateTimeImmutable($xml['to']);
        $this->period = (int)$xml['period'];
        $this->symbol = new Symbol($xml->symbol);
        $this->windDirection = new WindDirection($xml->windDirection);
        $this->windSpeed= new WindSpeed($xml->windSpeed);
        $this->temperature = new Temperature($xml->temperature);
        $this->pressure = new Pressure($xml->pressure);
    }

    /**
     * Returns the which index in the forecast
     * its for. 
     */
    public function getPeriod(): int {
        return $this->period;
    }

    /**
     * Returns the time this forecast is from
     *
     * @return \DateTimeImmutable
     */
    public function getFrom(): \DateTimeImmutable {
        return $this->from;
    }

    /**
     * Returns the time this forecast is to/until.
     *
     * @return \DateTimeImmutable
     */
    public function getUntil(): \DateTimeImmutable {
        return $this->until;
    }

    /**
     * Returns the «symbol» (e.g Clody etc)
     *
     * @return Symbol
     */
    public function getSymbol(): Symbol {
        return $this->symbol;
    }

    /**
     * Returns the wind direction
     *
     * @return WindDirection
     */
    public function getWindDirection(): WindDirection {
        return $this->windDirection;
    }

    /**
     * Returns the wind speed
     *
     * @return WindSpeed
     */
    public function getWindSpeed(): WindSpeed {
        return $this->windSpeed;
    }

    /**
     * Returns the temperature
     *
     * @return Temperature
     */
    public function getTemperature(): Temperature {
        return $this->temperature;
    }

    /**
     * Returns the pressure
     *
     * @return Pressure
     */
    public function getPressure(): Pressure {
        return $this->pressure;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator(): \Generator {
        foreach ([
            $this->getSymbol(),
            $this->getWindDirection(),
            $this->getWindSpeed(),
            $this->getTemperature(),
            $this->getPressure(),
        ] as $entity)
            yield $entity;
    }

    public function __toString(): string {
        return sprintf(
            '%s - %s: %s, %s, %s, %s, %s',
            $this->from->format('H:i'), $this->until->format('H:i'),
            $this->symbol, $this->windDirection, $this->windSpeed,
            $this->temperature, $this->pressure
        );
    }

}
