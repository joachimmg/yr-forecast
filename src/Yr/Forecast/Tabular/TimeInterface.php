<?php

namespace App\Yr\Forecast\Tabular;

interface TimeInterface {
    function getFrom(): \DateTimeImmutable;
    function getUntil(): \DateTimeImmutable;
    function getIterator(): \Generator;
}

?>
