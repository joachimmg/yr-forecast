<?php

namespace App\Yr;

use App\Yr\Forecast\Location;
use App\Yr\Forecast\Tabular;

/**
 * Read forecast data from Yr.no for a specific location.
 *
 * Disclaimer: To use this package you are required
 * to print the credits from the method ```getCredit()```
 *
 * Require the package to your project by adding (or creating)
 * the following in the `composer.json`-file:
 * ```
 * {
 *      "require": {
 *          "joachimmg/yr-forecast": "dev-master"
 *      },
 *      "repositories": [{
 *          "type": "vcs",
 *          "url": "https://git.giaever.org/joachimmg/yr-forecast.git"
 *      }]
 * }
 * ```
 *
 * @author Joachim M. Giæver (joachim[]giaever.org)
 * @version 1.0
 */
final class Forecast {
    private $xml;
    private $location;

    /**
     * @param string $url The XML url to load data from
     */
    public function __construct(string $url) {
        $this->xml = simplexml_load_file($url);
        $this->location = new Location($this->xml->location);
    }

    /**
     * Returns the location data for the forecast
     *
     * @return App\Forecast\Location
     */
    public function getLocation(): Location {
        return $this->location;
    }

    /**
     * Return the credit to Yr.no, Meterogical Institute and NRK
     *
     * @return array
     */
    final public function getCredit(): array {
        return [
            'text' => (string)$this->xml->credit->link->attributes()['text'],
            'url' => (string)$this->xml->credit->link->attributes()['url']
        ];
    }

    /**
     * Returns the time when the sun rise for the location
     *
     * @return \DateTimeImmutable
     */
    public function getSunrise(): \DateTimeImmutable {
        return new \DateTimeImmutable((string)$this->xml->sun['rise']);
    }

    /**
     * Returns the time when the sun sets for the location
     *
     * @return DateTimeImmutable
     */
    public function getSunset(): \DateTimeImmutable {
        return new \DateTimeImmutable((string)$this->xml->sun['set']);
    }

    /**
     * Returns links for forecasts in other formats
     *
     * @return \Generator
     */
    public function getLinks(): \Generator {
        foreach ($this->xml->links->children() as $link)
            yield [
                'id' => (string)$link->attributes()['id'],
                'url' => (string)$link->attributes()['url'],
            ];
    }

    /**
     * Return the time when this forecast was last update
     *
     * @return \DateTimeImmutable
     */
    public function getLastUpdate(): \DateTimeImmutable {
        return new \DateTimeImmutable((string)$this->xml->meta->lastupdate);
    }

    /**
     * Return the time when this forecast will update next
     *
     * @return \DateTimeImmutable
     */
    public function getNextUpdate(): \DateTimeImmutable {
        return new \DateTimeImmutable((string)$this->xml->meta->nextupdate);
    }

    /**
     * Get the forecast data
     *
     * @return Tabular
     */
    public function getTabular(): Tabular {
        return new Tabular($this, $this->xml->forecast->tabular);
    }

}

?>
