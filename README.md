
Yr Forecast
===

# Table of content    
1. [App](#app "App")
	1. [Yr](#appyr "App\Yr")
		1. [Forecast](#appyrforecast "App\Yr\Forecast")
			1. [Location](#location-appyrforecast "App\Yr\Forecast\Location") Get information about the location,such as name, timezone and geodata
			2. [Tabular](#appyrforecasttabular "App\Yr\Forecast\Tabular")
				1. [Time](#appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time")
					1. [AbstractUnit](#abstractunit-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\AbstractUnit") Time-object entity should inherit this
					2. [ConvertableInterface](#convertableinterface-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\ConvertableInterface") Implemented on units that can be converted,such as wind speed and temperature.
					3. [CustomUnit](#customunit-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\CustomUnit") Custom unit, only used to operate on units of same kind
					4. [DiffInterface](#diffinterface-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\DiffInterface") Defines that an entity can be checked for differencesagains another entity.
					5. [Pressure](#pressure-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\Pressure") Airpressure
					6. [Symbol](#symbol-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\Symbol") Contains the sky data, e.g «Clody» etc
					7. [Temperature](#temperature-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\Temperature") Temperature
					8. [WindDirection](#winddirection-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\WindDirection") Wind direction
					9. [WindSpeed](#windspeed-appyrforecasttabulartime "App\Yr\Forecast\Tabular\Time\WindSpeed") Wind speed
				2. [Variation](#appyrforecasttabularvariation "App\Yr\Forecast\Tabular\Variation")
					1. [Variation](#variation-appyrforecasttabularvariation "App\Yr\Forecast\Tabular\Variation\Variation")
				3. [Statistics](#statistics-appyrforecasttabular "App\Yr\Forecast\Tabular\Statistics") Make simple statistic on analysed time objects,such as highest/lowest wind speed and temperature,average wind speed and temperature etc.
				4. [Time](#time-appyrforecasttabular "App\Yr\Forecast\Tabular\Time") Forecast data witin a time period
				5. [Variations](#variations-appyrforecasttabular "App\Yr\Forecast\Tabular\Variations") Removes superfluous forecast data in an Time-objectonly storing changes.
			3. [Tabular](#tabular-appyrforecast "App\Yr\Forecast\Tabular") Holds the forecast data in Time-objects.
		2. [Forecast](#forecast-appyr "App\Yr\Forecast") Read forecast data from Yr.no for a specific location.


---
Documentation
---

# App

 * [App\Yr](#appyr "Namespace: App\Yr")
 * [App\Yr\Forecast](#appyrforecast "Namespace: App\Yr\Forecast")
 * [App\Yr\Forecast\Tabular](#appyrforecasttabular "Namespace: App\Yr\Forecast\Tabular")
 * [App\Yr\Forecast\Tabular\Time](#appyrforecasttabulartime "Namespace: App\Yr\Forecast\Tabular\Time")
 * [App\Yr\Forecast\Tabular\Variation](#appyrforecasttabularvariation "Namespace: App\Yr\Forecast\Tabular\Variation")

# App\Yr

## Classes

### Forecast `App\Yr`


* Class is final

Read forecast data from Yr.no for a specific location.

Disclaimer: To use this package you are required
to print the credits from the method ```getCredit()```

Require the package to your project by adding (or creating)
the following in the `composer.json`-file:
```
{
     "require": {
         "joachimmg/yr-forecast": "dev-master"
     },
     "repositories": [{
         "type": "vcs",
         "url": "https://git.giaever.org/joachimmg/yr-forecast.git"
     }]
}
```

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecast)||public| |
|[getLocation](#getlocation-appyrforecast)|[Location](#location-appyrforecast)|public| Returns the location data for the forecast|
|[getCredit](#getcredit-appyrforecast)|array|final public| Return the credit to Yr.no, Meterogical Institute and NRK|
|[getSunrise](#getsunrise-appyrforecast)|[ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)|public| Returns the time when the sun rise for the location|
|[getSunset](#getsunset-appyrforecast)|[ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)|public| Returns the time when the sun sets for the location|
|[getLinks](#getlinks-appyrforecast)|[enerator](https://www.google.no/search?q=Generator)|public| Returns links for forecasts in other formats|
|[getLastUpdate](#getlastupdate-appyrforecast)|[ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)|public| Return the time when this forecast was last update|
|[getNextUpdate](#getnextupdate-appyrforecast)|[ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)|public| Return the time when this forecast will update next|
|[getTabular](#gettabular-appyrforecast)|[Tabular](#tabular-appyrforecast)|public| Get the forecast data|

#### Method details

##### __construct `App\Yr\Forecast`
```php
public function __construct(string $url);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|string|$url|The XML url to load data from|
---


##### getLocation `App\Yr\Forecast`
```php
public function getLocation();
```
 Returns the location data for the forecast


Returns: [Location](#location-appyrforecast)

---


##### getCredit `App\Yr\Forecast`
```php
final public function getCredit();
```
 Return the credit to Yr.no, Meterogical Institute and NRK


Returns: array

---


##### getSunrise `App\Yr\Forecast`
```php
public function getSunrise();
```
 Returns the time when the sun rise for the location


Returns: [ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)

---


##### getSunset `App\Yr\Forecast`
```php
public function getSunset();
```
 Returns the time when the sun sets for the location


Returns: [ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)

---


##### getLinks `App\Yr\Forecast`
```php
public function getLinks();
```
 Returns links for forecasts in other formats


Returns: [enerator](https://www.google.no/search?q=Generator)

---


##### getLastUpdate `App\Yr\Forecast`
```php
public function getLastUpdate();
```
 Return the time when this forecast was last update


Returns: [ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)

---


##### getNextUpdate `App\Yr\Forecast`
```php
public function getNextUpdate();
```
 Return the time when this forecast will update next


Returns: [ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)

---


##### getTabular `App\Yr\Forecast`
```php
public function getTabular();
```
 Get the forecast data


Returns: [Tabular](#tabular-appyrforecast)

---

# App\Yr\Forecast

## Classes

### Location `App\Yr\Forecast`


* Class is final

Get information about the location,
such as name, timezone and geodata

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecastlocation)||public| |
|[getName](#getname-appyrforecastlocation)|string|public| Get the location name, e.g «Tromsø»|
|[getType](#gettype-appyrforecastlocation)|string|public| Get the location type, e.g «City - large town»|
|[getCountry](#getcountry-appyrforecastlocation)|string|public| Get the location country|
|[getTimeZone](#gettimezone-appyrforecastlocation)|[ateTimeZone](https://www.google.no/search?q=DateTimeZone)|public| Get the location timezone|
|[getGeoData](#getgeodata-appyrforecastlocation)|array|public| Returns the geodata, in the format```[     'lat' => (float),     'lng' => (float),     'alt' => (float),]```|

#### Method details

##### __construct `App\Yr\Forecast\Location`
```php
public function __construct(SimpleXMLElement $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement|$xml|The xml-part containing the location data|
---


##### getName `App\Yr\Forecast\Location`
```php
public function getName();
```
 Get the location name, e.g «Tromsø»


Returns: string

---


##### getType `App\Yr\Forecast\Location`
```php
public function getType();
```
 Get the location type, e.g «City - large town»


Returns: string

---


##### getCountry `App\Yr\Forecast\Location`
```php
public function getCountry();
```
 Get the location country


Returns: string

---


##### getTimeZone `App\Yr\Forecast\Location`
```php
public function getTimeZone();
```
 Get the location timezone


Returns: [ateTimeZone](https://www.google.no/search?q=DateTimeZone)

---


##### getGeoData `App\Yr\Forecast\Location`
```php
public function getGeoData();
```
 Returns the geodata, in the format
```
[
     'lat' => (float),
     'lng' => (float),
     'alt' => (float),
]
```


Returns: array

---


### Tabular `App\Yr\Forecast`


* Class implements[IteratorAggregate](https://www.google.no/search?q=IteratorAggregate)

Holds the forecast data in Time-objects.

Use the ```getBetween``` option to limit the results.

Class also makes a simple statistic on the forecast
for the periode and a variation that will exclude
repetitive forecast data.

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabular)||public| |
|[addTime](#addtime-appyrforecasttabular)|[Tabular](#tabular-appyrforecast)|protected| Add a Time-object to the tabular|
|[getStatistics](#getstatistics-appyrforecasttabular)|[Statistics](#statistics-appyrforecasttabular)|public| Get statistics for the Time-object collection|
|[makeVariations](#makevariations-appyrforecasttabular)|[Tabular](#tabular-appyrforecast)|protected| |
|[getVariations](#getvariations-appyrforecasttabular)|[Variations](#variations-appyrforecasttabular)|public| Remove superfluous weather data.|
|[getBetween](#getbetween-appyrforecasttabular)|[Tabular](#tabular-appyrforecast)|public| Filter data between a certain periode, e.g```$forcast->between( $forcast->getSunset(), $forecast->getSunrise()->add(     new \DateInterval('P1D') ));```to only show from sunset today unti sunrise tomorrow|
|[getFrom](#getfrom-appyrforecasttabular)|[ateTimeInterface](https://www.google.no/search?q=DateTimeInterface)|public| Return the time this tabular is from|
|[getUntil](#getuntil-appyrforecasttabular)|[ateTimeInterface](https://www.google.no/search?q=DateTimeInterface)|public| Return the time this tabular is until|
|[getIterator](#getiterator-appyrforecasttabular)|[enerator](https://www.google.no/search?q=Generator)|public| {@inheritDoc}|

#### Method details

##### __construct `App\Yr\Forecast\Tabular`
```php
public function __construct(SimpleXMLElement null $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement null|$xml|The xml part holding the time objects, can be null|
---


##### addTime `App\Yr\Forecast\Tabular`
```php
protected function addTime(Time $time);
```
 Add a Time-object to the tabular


Parameters

| Type | Variable | Description |
|---|---|---|
|[Time](#tabular-appyrforecast)|$time|*None*|

Returns: [Tabular](#tabular-appyrforecast)

---


##### getStatistics `App\Yr\Forecast\Tabular`
```php
public function getStatistics();
```
 Get statistics for the Time-object collection


Returns: [Statistics](#statistics-appyrforecasttabular)

---


##### makeVariations `App\Yr\Forecast\Tabular`
```php
protected function makeVariations();
```

Returns: [Tabular](#tabular-appyrforecast)

---


##### getVariations `App\Yr\Forecast\Tabular`
```php
public function getVariations();
```
 Remove superfluous weather data.

Checks if the data in the Time-object differs from
the current Time-object and returns the unique data


Returns: [Variations](#variations-appyrforecasttabular)

---


##### getBetween `App\Yr\Forecast\Tabular`
```php
public function getBetween(DateTimeInterface $from, DateTimeInterface $until);
```
 Filter data between a certain periode, e.g
```
$forcast->between(
 $forcast->getSunset(),
 $forecast->getSunrise()->add(
     new \DateInterval('P1D')
 )
);
```
to only show from sunset today unti sunrise tomorrow


Parameters

| Type | Variable | Description |
|---|---|---|
|DateTimeInterface|$from|*None*|
|DateTimeInterface|$until|*None*|

Returns: [Tabular](#tabular-appyrforecast)

---


##### getFrom `App\Yr\Forecast\Tabular`
```php
public function getFrom();
```
 Return the time this tabular is from


Returns: [ateTimeInterface](https://www.google.no/search?q=DateTimeInterface)

---


##### getUntil `App\Yr\Forecast\Tabular`
```php
public function getUntil();
```
 Return the time this tabular is until


Returns: [ateTimeInterface](https://www.google.no/search?q=DateTimeInterface)

---


##### getIterator `App\Yr\Forecast\Tabular`
```php
public function getIterator();
```
 {@inheritDoc}


Returns: [enerator](https://www.google.no/search?q=Generator)

---

# App\Yr\Forecast\Tabular

## Classes

### Statistics `App\Yr\Forecast\Tabular`

Make simple statistic on analysed time objects,
such as highest/lowest wind speed and temperature,
average wind speed and temperature etc.

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabularstatistics)||public| |
|[analyse](#analyse-appyrforecasttabularstatistics)|[Statistics](#statistics-appyrforecasttabular)|public| Analyse a single Time-object|
|[getAverageTemperature](#getaveragetemperature-appyrforecasttabularstatistics)|[AbstractUnit](#abstractunit-appyrforecasttabulartime)|public| |
|[getAverageWindSpeed](#getaveragewindspeed-appyrforecasttabularstatistics)|[AbstractUnit](#abstractunit-appyrforecasttabulartime)|public| |
|[getHighestTemperature](#gethighesttemperature-appyrforecasttabularstatistics)|[Temperature](#temperature-appyrforecasttabulartime)|public| |
|[getLowestTemperature](#getlowesttemperature-appyrforecasttabularstatistics)|[Temperature](#temperature-appyrforecasttabulartime)|public| |
|[getHighestWindspeed](#gethighestwindspeed-appyrforecasttabularstatistics)|[WindSpeed](#windspeed-appyrforecasttabulartime)|public| |
|[getLowestWindSpeed](#getlowestwindspeed-appyrforecasttabularstatistics)|[WindSpeed](#windspeed-appyrforecasttabulartime)|public| |
|[getAverageSymbols](#getaveragesymbols-appyrforecasttabularstatistics)|array|public| |
|[getMostCommonSymbol](#getmostcommonsymbol-appyrforecasttabularstatistics)|array|public| |

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Statistics`
```php
public function __construct();
```
---


##### analyse `App\Yr\Forecast\Tabular\Statistics`
```php
public function analyse(Time $t);
```
 Analyse a single Time-object


Parameters

| Type | Variable | Description |
|---|---|---|
|[Time](#tabular-appyrforecast)|$t|The time object.|

Returns: [Statistics](#statistics-appyrforecasttabular)

---


##### getAverageTemperature `App\Yr\Forecast\Tabular\Statistics`
```php
public function getAverageTemperature();
```

Returns: [AbstractUnit](#abstractunit-appyrforecasttabulartime)

---


##### getAverageWindSpeed `App\Yr\Forecast\Tabular\Statistics`
```php
public function getAverageWindSpeed();
```

Returns: [AbstractUnit](#abstractunit-appyrforecasttabulartime)

---


##### getHighestTemperature `App\Yr\Forecast\Tabular\Statistics`
```php
public function getHighestTemperature();
```

Returns: [Temperature](#temperature-appyrforecasttabulartime)

---


##### getLowestTemperature `App\Yr\Forecast\Tabular\Statistics`
```php
public function getLowestTemperature();
```

Returns: [Temperature](#temperature-appyrforecasttabulartime)

---


##### getHighestWindspeed `App\Yr\Forecast\Tabular\Statistics`
```php
public function getHighestWindspeed();
```

Returns: [WindSpeed](#windspeed-appyrforecasttabulartime)

---


##### getLowestWindSpeed `App\Yr\Forecast\Tabular\Statistics`
```php
public function getLowestWindSpeed();
```

Returns: [WindSpeed](#windspeed-appyrforecasttabulartime)

---


##### getAverageSymbols `App\Yr\Forecast\Tabular\Statistics`
```php
public function getAverageSymbols();
```

Returns: array

---


##### getMostCommonSymbol `App\Yr\Forecast\Tabular\Statistics`
```php
public function getMostCommonSymbol();
```

Returns: array

---


### Time `App\Yr\Forecast\Tabular`


* Class is final
* Class implements[IteratorAggregate](https://www.google.no/search?q=IteratorAggregate)

Forecast data witin a time period

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabulartime)||public| |
|[getPeriod](#getperiod-appyrforecasttabulartime)|int|public| Returns the which index in the forecastits for.|
|[getFrom](#getfrom-appyrforecasttabulartime)|[ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)|public| Returns the time this forecast is from|
|[getUntil](#getuntil-appyrforecasttabulartime)|[ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)|public| Returns the time this forecast is to/until.|
|[getSymbol](#getsymbol-appyrforecasttabulartime)|[Symbol](#symbol-appyrforecasttabulartime)|public| Returns the «symbol» (e.g Clody etc)|
|[getWindDirection](#getwinddirection-appyrforecasttabulartime)|[WindDirection](#winddirection-appyrforecasttabulartime)|public| Returns the wind direction|
|[getWindSpeed](#getwindspeed-appyrforecasttabulartime)|[WindSpeed](#windspeed-appyrforecasttabulartime)|public| Returns the wind speed|
|[getTemperature](#gettemperature-appyrforecasttabulartime)|[Temperature](#temperature-appyrforecasttabulartime)|public| Returns the temperature|
|[getPressure](#getpressure-appyrforecasttabulartime)|[Pressure](#pressure-appyrforecasttabulartime)|public| Returns the pressure|
|[getIterator](#getiterator-appyrforecasttabulartime)|[enerator](https://www.google.no/search?q=Generator)|public| {@inheritDoc}|
|[__toString](#__tostring-appyrforecasttabulartime)|string|public| |

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Time`
```php
public function __construct(SimpleXMLElement $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement|$xml|The xml holding the time-object|
---


##### getPeriod `App\Yr\Forecast\Tabular\Time`
```php
public function getPeriod();
```
 Returns the which index in the forecast
its for.


Returns: int

---


##### getFrom `App\Yr\Forecast\Tabular\Time`
```php
public function getFrom();
```
 Returns the time this forecast is from


Returns: [ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)

---


##### getUntil `App\Yr\Forecast\Tabular\Time`
```php
public function getUntil();
```
 Returns the time this forecast is to/until.


Returns: [ateTimeImmutable](https://www.google.no/search?q=DateTimeImmutable)

---


##### getSymbol `App\Yr\Forecast\Tabular\Time`
```php
public function getSymbol();
```
 Returns the «symbol» (e.g Clody etc)


Returns: [Symbol](#symbol-appyrforecasttabulartime)

---


##### getWindDirection `App\Yr\Forecast\Tabular\Time`
```php
public function getWindDirection();
```
 Returns the wind direction


Returns: [WindDirection](#winddirection-appyrforecasttabulartime)

---


##### getWindSpeed `App\Yr\Forecast\Tabular\Time`
```php
public function getWindSpeed();
```
 Returns the wind speed


Returns: [WindSpeed](#windspeed-appyrforecasttabulartime)

---


##### getTemperature `App\Yr\Forecast\Tabular\Time`
```php
public function getTemperature();
```
 Returns the temperature


Returns: [Temperature](#temperature-appyrforecasttabulartime)

---


##### getPressure `App\Yr\Forecast\Tabular\Time`
```php
public function getPressure();
```
 Returns the pressure


Returns: [Pressure](#pressure-appyrforecasttabulartime)

---


##### getIterator `App\Yr\Forecast\Tabular\Time`
```php
public function getIterator();
```
 {@inheritDoc}


Returns: [enerator](https://www.google.no/search?q=Generator)

---


##### __toString `App\Yr\Forecast\Tabular\Time`
```php
public function __toString();
```

Returns: string

---


### Variations `App\Yr\Forecast\Tabular`

Removes superfluous forecast data in an Time-object
only storing changes.

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabularvariations)||public| |
|[getTime](#gettime-appyrforecasttabularvariations)|[Time](#time-appyrforecasttabular)|public| |
|[getData](#getdata-appyrforecasttabularvariations)|array|public| Returns the changes in the forecast|

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Variations`
```php
public function __construct(array $t);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|array|$t|Array of Time-objects|
---


##### getTime `App\Yr\Forecast\Tabular\Variations`
```php
public function getTime();
```

Returns: [Time](#time-appyrforecasttabular)

---


##### getData `App\Yr\Forecast\Tabular\Variations`
```php
public function getData(callable $filterFn = null);
```
 Returns the changes in the forecast


Parameters

| Type | Variable | Description |
|---|---|---|
|callable|$filterFn|*None*|

Returns: array

---

# App\Yr\Forecast\Tabular\Time

## Classes

### AbstractUnit `App\Yr\Forecast\Tabular\Time`


* Class is abstact
* Class implements[App\Yr\Forecast\Tabular\Time\DiffInterface](#diffinterface-appyrforecasttabulartime)

Time-object entity should inherit this

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabulartimeabstractunit)||public| |
|[transformClass](#transformclass-appyrforecasttabulartimeabstractunit)|[AbstractUnit](#abstractunit-appyrforecasttabulartime)|protected| Transform custom unit to self|
|[add](#add-appyrforecasttabulartimeabstractunit)|[AbstractUnit](#abstractunit-appyrforecasttabulartime)|public| Addition method|
|[sub](#sub-appyrforecasttabulartimeabstractunit)|[AbstractUnit](#abstractunit-appyrforecasttabulartime)|public| Subtraction method|
|[mul](#mul-appyrforecasttabulartimeabstractunit)|[AbstractUnit](#abstractunit-appyrforecasttabulartime)|public| Multiplication method|
|[div](#div-appyrforecasttabulartimeabstractunit)|[AbstractUnit](#abstractunit-appyrforecasttabulartime)|public| Divide method|
|[getValue](#getvalue-appyrforecasttabulartimeabstractunit)|float|public| Get the value|
|[getUnit](#getunit-appyrforecasttabulartimeabstractunit)|string|public| Return the unit (e.g «degree»)|
|[diff](#diff-appyrforecasttabulartimeabstractunit)|int|public| Note! Working on floats, so operates on 100-times value.|
|[__toString](#__tostring-appyrforecasttabulartimeabstractunit)|string|public| |

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function __construct(float $value, string $unit);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|float|$value|The value|
|string|$unit|The unit|
---


##### transformClass `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
protected function transformClass(CustomUnit $cu);
```
 Transform custom unit to self


Parameters

| Type | Variable | Description |
|---|---|---|
|[CustomUnit](#time-appyrforecasttabular)|$cu|*None*|

Returns: [AbstractUnit](#abstractunit-appyrforecasttabulartime)

---


##### add `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function add(AbstractUnit $with);
```
 Addition method


Parameters

| Type | Variable | Description |
|---|---|---|
|[AbstractUnit](#time-appyrforecasttabular)|$with|Unit to add with|

Returns: [AbstractUnit](#abstractunit-appyrforecasttabulartime)

---


##### sub `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function sub(AbstractUnit $with);
```
 Subtraction method


Parameters

| Type | Variable | Description |
|---|---|---|
|[AbstractUnit](#time-appyrforecasttabular)|$with|Unit to sub with|

Returns: [AbstractUnit](#abstractunit-appyrforecasttabulartime)

---


##### mul `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function mul(AbstractUnit $with);
```
 Multiplication method


Parameters

| Type | Variable | Description |
|---|---|---|
|[AbstractUnit](#time-appyrforecasttabular)|$with|Unit to multiply with|

Returns: [AbstractUnit](#abstractunit-appyrforecasttabulartime)

---


##### div `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function div(AbstractUnit $with);
```
 Divide method


Parameters

| Type | Variable | Description |
|---|---|---|
|[AbstractUnit](#time-appyrforecasttabular)|$with|Unit to divide with|

Returns: [AbstractUnit](#abstractunit-appyrforecasttabulartime)

---


##### getValue `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function getValue();
```
 Get the value


Returns: float

---


##### getUnit `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function getUnit();
```
 Return the unit (e.g «degree»)


Returns: string

---


##### diff `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function diff(DiffInterface $d);
```
 Note! Working on floats, so operates on 100-times value.

{@inheritDoc}


Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$d|*None*|

Returns: int

---


##### __toString `App\Yr\Forecast\Tabular\Time\AbstractUnit`
```php
public function __toString();
```

Returns: string

---


### CustomUnit `App\Yr\Forecast\Tabular\Time`


* Class extends [App\Yr\Forecast\Tabular\Time\AbstractUnit](#abstractunit-appyrforecasttabulartime)
* Class implements[App\Yr\Forecast\Tabular\Time\DiffInterface](#diffinterface-appyrforecasttabulartime)

Custom unit, only used to operate on units of same kind

Classes should transform this (see {@AbstractUnit}) to
the correct class again after operation.


### Pressure `App\Yr\Forecast\Tabular\Time`


* Class extends [App\Yr\Forecast\Tabular\Time\AbstractUnit](#abstractunit-appyrforecasttabulartime)
* Class implements[App\Yr\Forecast\Tabular\Time\DiffInterface](#diffinterface-appyrforecasttabulartime)

Airpressure

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabulartimepressure)||public| |
|[isLowPressure](#islowpressure-appyrforecasttabulartimepressure)|bool|public| Check if the pressure is below normal pressure|
|[isHighPressure](#ishighpressure-appyrforecasttabulartimepressure)|bool|public| Check if the pressure is above normal pressure|
|[diff](#diff-appyrforecasttabulartimepressure)|int|public| Note! Working on floats, so operates on 100-times value.|

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Time\Pressure`
```php
public function __construct(SimpleXMLElement $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement|$xml|XML containing the pressure|
---


##### isLowPressure `App\Yr\Forecast\Tabular\Time\Pressure`
```php
public function isLowPressure();
```
 Check if the pressure is below normal pressure


Returns: bool

---


##### isHighPressure `App\Yr\Forecast\Tabular\Time\Pressure`
```php
public function isHighPressure();
```
 Check if the pressure is above normal pressure


Returns: bool

---


##### diff `App\Yr\Forecast\Tabular\Time\Pressure`
```php
public function diff(DiffInterface $d);
```
 Note! Working on floats, so operates on 100-times value.

{@inheritDoc}


Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$d|*None*|

Returns: int

---


### Symbol `App\Yr\Forecast\Tabular\Time`


* Class implements[App\Yr\Forecast\Tabular\Time\DiffInterface](#diffinterface-appyrforecasttabulartime)

Contains the sky data, e.g «Clody» etc

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabulartimesymbol)||public| |
|[getNumber](#getnumber-appyrforecasttabulartimesymbol)|int|public| Retuns the type identifier|
|[getName](#getname-appyrforecasttabulartimesymbol)|string|public| Returns the name, e.g «clody».|
|[getVar](#getvar-appyrforecasttabulartimesymbol)|string|public| Return the var-variable|
|[diff](#diff-appyrforecasttabulartimesymbol)|int|public| Check for differences with another DiffInterface.|
|[__toString](#__tostring-appyrforecasttabulartimesymbol)|string|public| |

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Time\Symbol`
```php
public function __construct(SimpleXMLElement $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement|$xml|XML containing the symbol data|
---


##### getNumber `App\Yr\Forecast\Tabular\Time\Symbol`
```php
public function getNumber();
```
 Retuns the type identifier


Returns: int

---


##### getName `App\Yr\Forecast\Tabular\Time\Symbol`
```php
public function getName();
```
 Returns the name, e.g «clody».


Returns: string

---


##### getVar `App\Yr\Forecast\Tabular\Time\Symbol`
```php
public function getVar();
```
 Return the var-variable


Returns: string

---


##### diff `App\Yr\Forecast\Tabular\Time\Symbol`
```php
public function diff(DiffInterface $s);
```
 Check for differences with another DiffInterface.

The method should check that the objects is the same.


Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$s|*None*|

Returns: int

---


##### __toString `App\Yr\Forecast\Tabular\Time\Symbol`
```php
public function __toString();
```

Returns: string

---


### Temperature `App\Yr\Forecast\Tabular\Time`


* Class extends [App\Yr\Forecast\Tabular\Time\AbstractUnit](#abstractunit-appyrforecasttabulartime)
* Class implements
	* [App\Yr\Forecast\Tabular\Time\ConvertableInterface](#convertableinterface-appyrforecasttabulartime)
	* [App\Yr\Forecast\Tabular\Time\DiffInterface](#diffinterface-appyrforecasttabulartime)

Temperature

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabulartimetemperature)||public| |
|[convertTo](#convertto-appyrforecasttabulartimetemperature)|[ConvertableInterface](#convertableinterface-appyrforecasttabulartime)|public| Convert the to a different unit|
|[getDegree](#getdegree-appyrforecasttabulartimetemperature)|string|public| Return the ⁰X symbol|

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Time\Temperature`
```php
public function __construct(SimpleXMLElement null $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement null|$xml|XML containing the temperature|
---


##### convertTo `App\Yr\Forecast\Tabular\Time\Temperature`
```php
public function convertTo(string $unit);
```
 Convert the to a different unit


Parameters

| Type | Variable | Description |
|---|---|---|
|string|$unit|The unit to convert to, eg UNIT_FTS|

Returns: [ConvertableInterface](#convertableinterface-appyrforecasttabulartime)

---


##### getDegree `App\Yr\Forecast\Tabular\Time\Temperature`
```php
public function getDegree();
```
 Return the ⁰X symbol


Returns: string

---


### WindDirection `App\Yr\Forecast\Tabular\Time`


* Class extends [App\Yr\Forecast\Tabular\Time\AbstractUnit](#abstractunit-appyrforecasttabulartime)
* Class implements[App\Yr\Forecast\Tabular\Time\DiffInterface](#diffinterface-appyrforecasttabulartime)

Wind direction

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabulartimewinddirection)||public| |
|[getName](#getname-appyrforecasttabulartimewinddirection)|string|public| Returns the wind direction in full,e.g «Northeast».|
|[getDirection](#getdirection-appyrforecasttabulartimewinddirection)|int|public| Returns a direction on the compass, notin degree, but in interger between 1 - 16,each explining which spectre.|
|[diff](#diff-appyrforecasttabulartimewinddirection)|int|public| Note! Working on floats, so operates on 100-times value.|

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Time\WindDirection`
```php
public function __construct(SimpleXMLElement $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement|$xml|XML element containing the wind direction|
---


##### getName `App\Yr\Forecast\Tabular\Time\WindDirection`
```php
public function getName();
```
 Returns the wind direction in full,
e.g «Northeast».


Returns: string

---


##### getDirection `App\Yr\Forecast\Tabular\Time\WindDirection`
```php
public function getDirection();
```
 Returns a direction on the compass, not
in degree, but in interger between 1 - 16,
each explining which spectre.

E.g
 1: North,
 2: North-northeast,
 3: North-east,
 [...]
 9: South
etc..


Returns: int

---


##### diff `App\Yr\Forecast\Tabular\Time\WindDirection`
```php
public function diff(DiffInterface $d);
```
 Note! Working on floats, so operates on 100-times value.

{@inheritDoc}


Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$d|*None*|

Returns: int

---


### WindSpeed `App\Yr\Forecast\Tabular\Time`


* Class extends [App\Yr\Forecast\Tabular\Time\AbstractUnit](#abstractunit-appyrforecasttabulartime)
* Class implements
	* [App\Yr\Forecast\Tabular\Time\ConvertableInterface](#convertableinterface-appyrforecasttabulartime)
	* [App\Yr\Forecast\Tabular\Time\DiffInterface](#diffinterface-appyrforecasttabulartime)

Wind speed

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabulartimewindspeed)||public| |
|[convertTo](#convertto-appyrforecasttabulartimewindspeed)|[ConvertableInterface](#convertableinterface-appyrforecasttabulartime)|public| Convert the to a different unit|
|[getName](#getname-appyrforecasttabulartimewindspeed)|string|public| Returns the wind name, e.g «light breeze»|
|[setName](#setname-appyrforecasttabulartimewindspeed)|[WindSpeed](#windspeed-appyrforecasttabulartime)|protected| Used on conversion|
|[diff](#diff-appyrforecasttabulartimewindspeed)|int|public| Note! Working on floats, so operates on 100-times value.|
|[__toString](#__tostring-appyrforecasttabulartimewindspeed)|string|public| |

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Time\WindSpeed`
```php
public function __construct(SimpleXMLElement $xml);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|SimpleXMLElement|$xml|XML containing the wind spedd|
---


##### convertTo `App\Yr\Forecast\Tabular\Time\WindSpeed`
```php
public function convertTo(string $unit);
```
 Convert the to a different unit


Parameters

| Type | Variable | Description |
|---|---|---|
|string|$unit|The unit to convert to, eg UNIT_FTS|

Returns: [ConvertableInterface](#convertableinterface-appyrforecasttabulartime)

---


##### getName `App\Yr\Forecast\Tabular\Time\WindSpeed`
```php
public function getName();
```
 Returns the wind name, e.g «light breeze»


Returns: string

---


##### setName `App\Yr\Forecast\Tabular\Time\WindSpeed`
```php
protected function setName(string $name);
```
 Used on conversion


Parameters

| Type | Variable | Description |
|---|---|---|
|string|$name|*None*|

Returns: [WindSpeed](#windspeed-appyrforecasttabulartime)

---


##### diff `App\Yr\Forecast\Tabular\Time\WindSpeed`
```php
public function diff(DiffInterface $e);
```
 Note! Working on floats, so operates on 100-times value.

{@inheritDoc}


Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$e|*None*|

Returns: int

---


##### __toString `App\Yr\Forecast\Tabular\Time\WindSpeed`
```php
public function __toString();
```

Returns: string

---

## Interfaces

### ConvertableInterface `App\Yr\Forecast\Tabular\Time`

Implemented on units that can be converted,
such as wind speed and temperature.

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[convertTo](#convertto-appyrforecasttabulartimeconvertableinterface)|[ConvertableInterface](#convertableinterface-appyrforecasttabulartime)|public| Convert the to a different unit|

#### Method details

##### convertTo `App\Yr\Forecast\Tabular\Time\ConvertableInterface`
```php
public function convertTo(string $unit);
```
 Convert the to a different unit


Parameters

| Type | Variable | Description |
|---|---|---|
|string|$unit|The unit to convert to, eg UNIT_FTS|

Returns: [ConvertableInterface](#convertableinterface-appyrforecasttabulartime)

---


### DiffInterface `App\Yr\Forecast\Tabular\Time`

Defines that an entity can be checked for differences
agains another entity.

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[diff](#diff-appyrforecasttabulartimediffinterface)|int|public| Check for differences with another DiffInterface.|

#### Method details

##### diff `App\Yr\Forecast\Tabular\Time\DiffInterface`
```php
public function diff(DiffInterface $e);
```
 Check for differences with another DiffInterface.

The method should check that the objects is the same.


Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$e|The object to check agains|

Returns: int

---

# App\Yr\Forecast\Tabular\Variation

## Classes

### Variation `App\Yr\Forecast\Tabular\Variation`


* Class implements[IteratorAggregate](https://www.google.no/search?q=IteratorAggregate)

#### Methods

|Name|Return|Access|Description|
|:---|:---|:---|:---|
|[__construct](#__construct-appyrforecasttabularvariationvariation)||public| |
|[addEntity](#addentity-appyrforecasttabularvariationvariation)|[Variation](#variation-appyrforecasttabularvariation)|public| |
|[getTime](#gettime-appyrforecasttabularvariationvariation)|[Time](#time-appyrforecasttabular)|public| |
|[getIntersection](#getintersection-appyrforecasttabularvariationvariation)|[DiffInterface](#diffinterface-appyrforecasttabularvariationappyrforecasttabulartime) ***v*** null|public| |
|[isEmpty](#isempty-appyrforecasttabularvariationvariation)|bool|public| |
|[getIterator](#getiterator-appyrforecasttabularvariationvariation)|[enerator](https://www.google.no/search?q=Generator)|public| |

#### Method details

##### __construct `App\Yr\Forecast\Tabular\Variation\Variation`
```php
public function __construct(Time $t);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|[Time](#tabular-appyrforecast)|$t|*None*|
---


##### addEntity `App\Yr\Forecast\Tabular\Variation\Variation`
```php
public function addEntity(DiffInterface $entity, DiffInterface null $intersects);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$entity|*None*|
|[DiffInterface](#time-appyrforecasttabular) null|$intersects|*None*|

Returns: [Variation](#variation-appyrforecasttabularvariation)

---


##### getTime `App\Yr\Forecast\Tabular\Variation\Variation`
```php
public function getTime();
```

Returns: [Time](#time-appyrforecasttabular)

---


##### getIntersection `App\Yr\Forecast\Tabular\Variation\Variation`
```php
public function getIntersection(DiffInterface $entity);
```

Parameters

| Type | Variable | Description |
|---|---|---|
|[DiffInterface](#time-appyrforecasttabular)|$entity|*None*|

Returns: [DiffInterface](#diffinterface-appyrforecasttabularvariationappyrforecasttabulartime) ***v*** null

---


##### isEmpty `App\Yr\Forecast\Tabular\Variation\Variation`
```php
public function isEmpty();
```

Returns: bool

---


##### getIterator `App\Yr\Forecast\Tabular\Variation\Variation`
```php
public function getIterator();
```

Returns: [enerator](https://www.google.no/search?q=Generator)

---


 - Genetated using Sami and the [Sami/Twig Markdown Extension](https://git.giaever.org/joachimmg/sami-markdown)

